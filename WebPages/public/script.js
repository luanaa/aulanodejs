// script.js

document.addEventListener('DOMContentLoaded', function () {
    // Exemplo: Alterar o texto de um elemento ao carregar a página
    const element = document.getElementById('exemploTexto');
  
    if (element) {
      element.textContent = 'Texto alterado pelo script.js!';
    }
  });
  